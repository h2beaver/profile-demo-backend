package h2beaver.demo.git;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import h2beaver.demo.Settings;
import h2beaver.demo.data.ProfileMetadata;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.util.FS;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;

public class Profile {

    private static Profile instance = init("profile-taoyuan-vector");

    private static TransportConfigCallback configCallback = new TransportConfigCallback() {
        @Override
        public void configure(final Transport transport) {
            SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(new JschConfigSessionFactory() {

                @Override
                protected JSch getJSch(final OpenSshConfig.Host hc, FS fs) throws JSchException {
                    JSch jsch = super.getJSch(hc, fs);
                    jsch.removeAllIdentity();
                    jsch.addIdentity(Settings.GIT_SSH_FILE, Settings.GIT_SSH_PASSPHRASE);
                    return jsch;
                }

                @Override
                protected void configure(final OpenSshConfig.Host hc, final Session session) {
                    session.setConfig("StrictHostKeyChecking", "no");
                }
            });
        }
    };

    private static Profile init(String path) {
        try {
            FileRepositoryBuilder builder = new FileRepositoryBuilder();
            Repository repository = builder.findGitDir(Paths.get(Settings.REPO_BASEDIR, path).toFile())
                    .readEnvironment() // scan environment GIT_* variables
                    .build();
            return new Profile(repository);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    public static Profile getInstance() {
        return instance;
    }

    private Repository repository;

    private Profile(Repository repository) {
        this.repository = repository;
    }

    public String addNewProfiles(ProfileMetadata meta, Map<String, h2beaver.demo.data.Profile> data) throws IOException, GitAPIException {
        ObjectMapper mapper = new ObjectMapper();
        String branch = "b" + System.currentTimeMillis();
        Git git = new Git(this.repository);
        File upload = new File(this.repository.getWorkTree(), "upload");
        upload.mkdir();

        git.checkout().setName("master").call();
        git.pull().setTransportConfigCallback(configCallback).call();

        git.checkout().setCreateBranch(true).setName(branch).call();

        mapper.writeValue(new File(upload, "metadata.json"), meta);
        for (Iterator<Map.Entry<String, h2beaver.demo.data.Profile>> iterator = data.entrySet()
                .iterator(); iterator.hasNext(); ) {
            Map.Entry<String, h2beaver.demo.data.Profile> e = iterator.next();
            mapper.writeValue(new File(upload, e.getKey() + ".json"), e.getValue());
        }
        git.add().addFilepattern("upload/").call();
        git.commit().setMessage("Commit uploaded excel data - " + branch).call();
        git.push().setTransportConfigCallback(configCallback).call();
        git.checkout().setName("master").call();
        return branch;
    }

}
