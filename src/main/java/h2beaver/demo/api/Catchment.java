package h2beaver.demo.api;

import h2beaver.demo.Settings;
import h2beaver.demo.data.DataVersion;
import h2beaver.demo.data.PipelineVariables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Path("catchment")
public class Catchment {
    private static String PROJECT_TARGET = "https://h2-gitlab.pointing.tw/api/v4/projects/3";
    private static String[] PARAMETERS = {"height", "width", "threshold", "maxslope", "fill"};

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @POST
    @Path("run")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response startCatchmentPipeline(Form input) {
        MultivaluedMap<String,String> parameters = input.asMap();

        PipelineVariables data = new PipelineVariables();
        data.ref = Optional.ofNullable(parameters.getFirst("ref")).orElse("run");

        Arrays.stream(PARAMETERS).forEachOrdered(key -> {
            String value = parameters.getFirst(key);
            if (value != null) {
                data.variables.add(new PipelineVariables.Variable(key, value));
            }
        });
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(PROJECT_TARGET);
        Response gitlabResponse = webTarget
                .path("pipeline")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header("PRIVATE-TOKEN", Settings.GITLAB_ACCESS_TOKEN)
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));
        return gitlabResponse;
    }

    private Map getPipelineBuildJob(String id) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget
                = client.target(PROJECT_TARGET);
        Map[] r = webTarget
                .path("pipelines/" + id + "/jobs")
                .request(MediaType.APPLICATION_JSON)
                .header("PRIVATE-TOKEN", Settings.GITLAB_ACCESS_TOKEN)
                .get(Map[].class);
        if (r == null) {
            return null;
        }
        return Arrays.stream(r).filter(m -> "build".equals(m.get("stage"))).findFirst().orElse(null);
    }

    private String getPipelineArtifact(String id, String filename) {
        Map r = getPipelineBuildJob(id);
        if (r == null) return null;
        if ("success".equals(r.get("status"))) {
            String jobId = r.get("id").toString();
            Client client = ClientBuilder.newClient();
            WebTarget webTarget
                    = client.target(PROJECT_TARGET);
            return webTarget
                    .path("jobs/" + jobId + "/artifacts/workspace/" + filename)
                    .request()
                    .header("PRIVATE-TOKEN", Settings.GITLAB_ACCESS_TOKEN)
                    .get(String.class);
        } else {
            DataVersion v = DataVersion.fromPipelineData(r);
        }
        return null;
    }

    @GET
    @Path("{id}/status")
    @Produces(MediaType.APPLICATION_JSON)
    public DataVersion getPipelineStatus(@PathParam("id") String id) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget
                = client.target(PROJECT_TARGET);
        Map r = webTarget
                .path("pipelines/" + id)
                .request(MediaType.APPLICATION_JSON)
                .header("PRIVATE-TOKEN", Settings.GITLAB_ACCESS_TOKEN)
                .get(Map.class);
        if (r == null) {
            return null;
        }
        DataVersion v =  DataVersion.fromPipelineData(r);
        if (v != null) return v;
        response.setStatus(404);
        return null;
    }

    @GET
    @Path("/{id}/stream")
    @Produces(MediaType.APPLICATION_JSON)
    public String getStreamGeojson(@PathParam("id") String id) {
        return getPipelineArtifact(id, "h2__stream.geojson");
    }

    @GET
    @Path("/{id}/source_stream")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSourceStreamGeojson(@PathParam("id") String id) {
        Map<String, Object> parameters = getParameters(id);
        String source = (String)parameters.get("source");
        if (source == null) return null;
        return getPipelineArtifact(id, source);
    }

    @GET
    @Path("/{id}/basin")
    @Produces(MediaType.APPLICATION_JSON)
    public String getBasinGeojson(@PathParam("id") String id) {
        return getPipelineArtifact(id, "h2__basin.geojson");
    }

    private Integer tryParse(String s) {
        try {
            return Integer.valueOf(s);
        } catch (Exception ex) {
            return null;
        }
    }

    @GET
    @Path("/{id}/parameters")
    @Produces(MediaType.APPLICATION_JSON)
    public Map getParameters(@PathParam("id") String id) {
        Map<String, Object> json = new HashMap<>();
        String parameters = getPipelineArtifact(id, "parameters.txt");
        String[] lines = parameters.split("\\n");
        if (lines.length > 1) {
            json.put("source", lines[1]);
        }
        String[] parts = lines[0].split("\\s+");
        for (int i = 0; i < parts.length - 1; i += 2) {
            switch (parts[i]) {
                case "-h":
                    json.put("height", tryParse(parts[i + 1]));
                    break;
                case "-w":
                    json.put("width", tryParse(parts[i + 1]));
                    break;
                case "-t":
                    json.put("threshold", tryParse(parts[i + 1]));
                    break;
                case "-m":
                    json.put("maxslope", tryParse(parts[i + 1]));
                    break;
                case "-f":
                    json.put("fill", parts[i + 1]);
                    break;
            }
        }
        return json;
    }
}
