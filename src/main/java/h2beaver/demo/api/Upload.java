package h2beaver.demo.api;

import h2beaver.demo.Settings;
import h2beaver.demo.data.DataVersion;
import h2beaver.demo.data.Profile;
import h2beaver.demo.data.ProfileMetadata;
import org.apache.poi.EmptyFileException;
import org.apache.poi.ss.usermodel.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


@Path("/")
public class Upload {
    private static Logger log = LoggerFactory.getLogger("H2BEAVER");

    private void setProfileLeftRightPoint(Row r, Profile p) {
        if ("L".equals(r.getCell(1).getStringCellValue())) {
            p.left.x = r.getCell(4).getNumericCellValue();
            p.left.y = r.getCell(3).getNumericCellValue();
        } else if ("R".equals(r.getCell(1).getStringCellValue())) {
            p.right.x = r.getCell(4).getNumericCellValue();
            p.right.y = r.getCell(3).getNumericCellValue();
        }
    }

    // Example of 2-rows profile data
    // NK-H01320-04260C	L	7623.69	2753581.75	275776.1875	185.6	2753571.9759	275778.6584	2018
    //	                R	7623.69	2753562.25	275781.1251	185.6	2753571.9759	275778.6584	2018
    private Map<String, Profile> getStreamAndProfilePoint(Sheet dataSheet) {
        Map<String, Profile> data = new LinkedHashMap<>();
        Cell A1 = dataSheet.getRow(0).getCell(0);
        if ("斷面編號".equals(A1.getStringCellValue())) {
            log.info("Data sheet loaded: " + dataSheet.getSheetName());
            for (int i = 2; i <= 5000; i += 2) {
                Row r1 = dataSheet.getRow(i);
                Row r2 = dataSheet.getRow(i + 1);
                if (r1 == null || r2 == null) break;
                Profile profile = new Profile();
                profile.no = r1.getCell(0).getStringCellValue();
                profile.mainChannelDistance = r1.getCell(2).getNumericCellValue();
                profile.elevation = r1.getCell(5).getNumericCellValue();
                profile.streamReference.x = r1.getCell(7).getNumericCellValue();
                profile.streamReference.y = r1.getCell(6).getNumericCellValue();
                Cell noteCell = r1.getCell(8);
                switch (noteCell.getCellType()) {
                    case STRING:
                        profile.note = noteCell.getStringCellValue();
                        break;
                    case NUMERIC:
                        profile.note = Double.toString(noteCell.getNumericCellValue());
                        break;
                }
                setProfileLeftRightPoint(r1, profile);
                setProfileLeftRightPoint(r2, profile);

                data.put(profile.no, profile);
                log.info("Row loaded: " + i + "," + profile.no);
            }
        }
        return data;
    }

    private void findAndUpdateProfileYZ(ProfileMetadata meta, Map<String, Profile> data, Sheet dataSheet) {
        Cell A1 = dataSheet.getRow(0).getCell(0);
        log.info("Data sheet loaded: " + dataSheet.getSheetName() + " for " + A1.getStringCellValue());
        int count = 0;
        for (int i = 1; i < 5000; i++) {
            Row r = dataSheet.getRow(i);
            if (r == null) break;
            Cell c = r.getCell(0);
            if (c == null || c.getCellType() == CellType._NONE || c.getCellType() == CellType.BLANK) break;
            String no = c.getStringCellValue();
            Profile profile = data.get(no);
            int rows = (int) r.getCell(2).getNumericCellValue();
            List<double[]> table = new ArrayList<>();
            for (int j = 0; j < rows; j++) {
                i++;
                Row d = dataSheet.getRow(i);
                Cell c1 = d.getCell(1);
                Cell c2 = d.getCell(2);
                table.add(new double[]{c1.getNumericCellValue(), c2.getNumericCellValue()});
            }
            if (profile != null) {
                count++;
                log.info("Profile YZ data loaded for " + profile.no);
                profile.table = table.toArray(new double[][]{});
            }
        }
        meta.regions.put(A1.getStringCellValue(), count);
    }

    private void generateErrorMessage(String fileType, String filename, Exception ex) {
        log.warn("Excel parsing error " + fileType, ex);
        StringBuilder message = new StringBuilder(filename);
        message.append(fileType);
        message.append("檔案讀取錯誤：");
        if (ex instanceof GitAPIException) {
            message.append("資料寫入暫存區失敗，請連繫系統管理員");
        } else if (ex instanceof EmptyFileException || ex instanceof IOException) {
            message.append("無法判讀為 Excel 檔案格式");
        } else {
            message.append("無法自檔案中正確擷取");
            message.append(fileType);
            message.append("資料");
        }

        Response.ResponseBuilder builder = Response.status(Response.Status.BAD_REQUEST);
        builder.entity(new DataVersion(DataVersion.Status.Error, null, message.toString()));
        throw new WebApplicationException(builder.build());
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public DataVersion uploadFile(
            @FormDataParam("file1") InputStream profileNodeInputStream,
            @FormDataParam("file1") FormDataContentDisposition profileNodeDetails,
            @FormDataParam("file2") InputStream profileDataInputStream,
            @FormDataParam("file2") FormDataContentDisposition profileDataDetails) throws Exception {
        ProfileMetadata meta = new ProfileMetadata();
        Map<String, Profile> data = new LinkedHashMap<>();
        String fileName1 = profileNodeDetails.getFileName();
        String fileName2 = profileDataDetails.getFileName();
        try {
            if (fileName1 != null) {
                Workbook wb = WorkbookFactory.create(profileNodeInputStream);
                log.info(fileName1 + " has " + wb.getNumberOfSheets() + " sheets;");
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    data.putAll(getStreamAndProfilePoint(wb.getSheetAt(i)));
                }
            }
        } catch (Exception ex) {
            generateErrorMessage("座標", fileName1, ex);
        }

        try {
            if (fileName2 != null) {
                Workbook wb = WorkbookFactory.create(profileDataInputStream);
                log.info(fileName2 + " has " + wb.getNumberOfSheets() + " sheets;");
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    findAndUpdateProfileYZ(meta, data, wb.getSheetAt(i));
                }
            }

            h2beaver.demo.git.Profile repo = h2beaver.demo.git.Profile.getInstance();
            String branch = repo.addNewProfiles(meta, data);
            DataVersion v = new DataVersion(DataVersion.Status.OK, branch, new Date().toString());
            return v
                    .withProperty("streamReleaseVersions", Settings.STREAM_RELEASE_VERSIONS)
                    .withProperty("profileReleaseVersions", Settings.PROFILE_RELEASE_VERSIONS)
                    .withProperty("streamReleaseDownloadInfos", Settings.STREAM_RELEASE_DOWNLOAD_INFOS)
                    .withProperty("profileReleaseDownloadInfos", Settings.PROFILE_RELEASE_DOWNLOAD_INFOS);
        } catch (Exception ex) {
            generateErrorMessage("斷面", fileName2, ex);
        }

        return null;
    }
}
