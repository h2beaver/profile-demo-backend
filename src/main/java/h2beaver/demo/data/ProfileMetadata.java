package h2beaver.demo.data;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProfileMetadata {
    public final Date whenUpload = new Date();
    public Map<String, Integer> regions = new LinkedHashMap<>();
}
