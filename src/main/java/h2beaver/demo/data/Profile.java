package h2beaver.demo.data;

public class Profile {
    public String no;
    public Point left = new Point("Left");
    public Point right = new Point("Right");
    public Point streamReference = new Point("Stream");
    public double mainChannelDistance;
    public double elevation;
    public String note;
    public double[][] table;

    public class Point
    {
        private Point(String type) {
            this.type = type;
        }
        public String type;
        public double x;
        public double y;
    }
}
